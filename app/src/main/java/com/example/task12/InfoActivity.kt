package com.example.task12

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import java.util.jar.Attributes.Name

class InfoActivity : AppCompatActivity() {

    companion object {
        const val NAME = "name"
        const val SURNAME = "surname"
        const val SECONDNAME = "secondname"
        const val AGE = "age"
        const val HOBBY = "hobby"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        val name = intent.getStringExtra(NAME)
        val surname = intent.getStringExtra(SURNAME)
        val secondname = intent.getStringExtra(SECONDNAME)
        val age = intent.getIntExtra(AGE, 0)
        val hobby = intent.getStringExtra(HOBBY)

        val tvInfo = findViewById<TextView>(R.id.tvInfo)
        //tvInfo.text = "Name = $name. Surname = $surname. SecondName = $secondname. Age = $age. Hobby"

        when(age) {
            in 0..10 -> tvInfo.text = "Привет $surname $name $secondname!\n Тебе пока еще $age лет, но это не мешает иметь какое то хобби.\n Я знаю, что твое хобби: $hobby"
            in 11..20 -> tvInfo.text = "Привет $surname $name $secondname!\n Ты уже не маленький, твой возраст $age лет,\n и у тебя уже есть своё любимое дело - $hobby"
            in 21..40 -> tvInfo.text = "Привет $surname $name $secondname!\n Хобби является хорошим способом борьбы со стрессом!\n Тебе $age лет, и твоё любимое дело: $hobby"
            in 41..100 -> tvInfo.text = "Привет $surname $name $secondname!\n Хобби - это то, чем люди любят заниматься в свободное время,\n " +
                                                "и что наполняет их жизнь определенным смыслом!\n Тебе $age лет и в свободное время ты занимаешься: $hobby"
            else -> tvInfo.text = "Привет $surname $name $secondname! Тебе надо пересмотреть свой возраст!"

        }
    }
}