package com.example.task12

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val secondname = findViewById<TextView>(R.id.etSecondName)
        val age = findViewById<TextView>(R.id.etAge)
        val hobby = findViewById<TextView>(R.id.etHobby)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    fun onClickGo(view : View) {
        val intent = Intent(this, InfoActivity::class.java)

        val name = findViewById<TextView>(R.id.etName).text.toString()
        val surname = findViewById<TextView>(R.id.etSurname).text.toString()
        val secondname = findViewById<TextView>(R.id.etSecondName).text.toString()
        val age = findViewById<TextView>(R.id.etAge).text.toString().toInt()
        val hobby = findViewById<TextView>(R.id.etHobby).text.toString()

        intent.putExtra(InfoActivity.NAME, name)
        intent.putExtra(InfoActivity.SURNAME, surname)
        intent.putExtra(InfoActivity.SECONDNAME, secondname)
        intent.putExtra(InfoActivity.AGE, age)
        intent.putExtra(InfoActivity.HOBBY, hobby)

        startActivity(intent)
    }
}